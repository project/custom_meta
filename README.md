# Custom Meta

The module allows you to define and manage custom meta tags.
Simply select the Meta attribute (property, name, http-equiv) 
and set the name, label, and description of the tag.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/custom_meta).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/custom_meta).


## Requirements

This module requires the following modules:

- [Metatag](https://www.drupal.org/project/metatag)


## Installation

- Install as you would normally install a contributed Drupal module. Visit
  [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) 
  for further information.


## Configuration

- Configure the custom meta tags in 
  `Administration » Configuration » Search and Metadata » Metatag » Custom Meta Tags`:

    - Create the custom meta tags
    - The meta tags will be available for configuration in meta tag settings 
      under

      `Administration » Configuration » Search and Metadata » Metatag » Settings`


## FAQ

**Q: I have changed the "Meta attribute", but meta tag is not appearing. 
Is this normal?**

**A:** Yes, this is the intended behavior. you need to flush the cache to let 
it appear again.


## Maintainers

- Naveen Valecha - [naveenvalecha](https://www.drupal.org/u/naveenvalecha)

**This project has been sponsored by:**
- Morpht - Visit [Morpht](https://www.morpht.com) for more information.
