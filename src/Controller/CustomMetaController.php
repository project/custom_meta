<?php

namespace Drupal\custom_meta\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for custom meta routes.
 */
class CustomMetaController extends ControllerBase {

  /**
   * Displays the custom meta tags administration overview page.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function adminOverview() {
    $values = $this->config('custom_meta.settings')->get('tag') ?? [];
    // Table header.
    $header = [];
    $header[] = [
      'data' => $this->t('Name'),
      'field' => 'meta_name',
      'sort' => 'asc',
    ];
    $header[] = ['data' => $this->t('Label'), 'field' => 'meta_label'];
    $header[] = [
      'data' => $this->t('Description'),
      'field' => 'meta_description',
    ];
    $header[] = ['data' => $this->t('Attribute'), 'field' => 'meta_attribute'];
    $header[] = $this->t('Operations');

    $rows = [];
    $destination = $this->getDestinationArray();
    foreach ($values as $data) {
      // Table row.
      $row['data']['meta_name'] = $data['name'];
      $row['data']['meta_label'] = $data['label'];
      $row['data']['meta_description'] = $data['description'];
      $row['data']['meta_attribute'] = $data['attribute'];

      $operations = [];
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('custom_meta.admin_edit', ['id' => $data['name']], ['query' => $destination]),
      ];
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('custom_meta.delete', ['id' => $data['name']], ['query' => $destination]),
      ];
      $row['data']['operations'] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];

      $rows[] = $row;
    }

    $build['meta_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No custom meta tags available. <a href=":link">Add tag</a>.', [':link' => Url::fromRoute('custom_meta.admin_add')->toString()]),
    ];

    return $build;
  }

}
