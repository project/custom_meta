<?php

namespace Drupal\custom_meta\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the custom meta tag add form.
 */
class AddForm extends FormBase {

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $messenger;

  /**
   * Constructs a new AddForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_meta_admin_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $meta = [];
    if ($id) {
      $meta = $this->config('custom_meta.settings')->get('tag')[$id];
      $form['id'] = [
        '#type' => 'hidden',
        '#value' => $id,
      ];
    }

    $form['attribute'] = [
      '#type' => 'select',
      '#title' => $this->t('Meta attribute'),
      '#options' => [
        'name' => 'Name',
        'property' => 'Property',
        'http-equiv' => 'Http Equiv',
      ],
      '#description' => t('Specify custom meta tag attribute.'),
      '#required' => TRUE,
      '#default_value' => $meta['attribute'] ?? '',
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Meta name'),
      '#maxlength' => 255,
      '#description' => t('Specify custom meta tag name.'),
      '#required' => TRUE,
      '#default_value' => $meta['name'] ?? '',
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Meta label'),
      '#maxlength' => 255,
      '#description' => t('Specify custom meta tag label.'),
      '#required' => TRUE,
      '#default_value' => $meta['label'] ?? '',
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Meta description'),
      '#maxlength' => 255,
      '#description' => t('Specify custom meta tag description.'),
      '#required' => TRUE,
      '#default_value' => $meta['description'] ?? '',
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $existing_tags = $this->config('custom_meta.settings')->get('tag') ?? [];
    if (!$form_state->getValue('id') && array_key_exists($form_state->getValue('name'), $existing_tags)) {
      $form_state->setErrorByName('name', t('The custom meta tag %tag already exists.', ['%tag' => $form_state->getValue('name')]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $this->config('custom_meta.settings')->get('tag');
    $values[$form_state->getValue('name')] = [
      'attribute' => $form_state->getValue('attribute'),
      'name' => $form_state->getValue('name'),
      'label' => $form_state->getValue('label'),
      'description' => $form_state->getValue('description'),
    ];

    $this->configFactory()->getEditable('custom_meta.settings')
      ->set('tag', $values)
      ->save();
    $this->messenger->addStatus('Meta tag has been saved.');
    $form_state->setRedirect('custom_meta.admin_overview');
  }

}
