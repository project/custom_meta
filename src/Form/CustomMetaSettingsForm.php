<?php

namespace Drupal\custom_meta\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the custom meta settings form.
 */
class CustomMetaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_meta_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['custom_meta.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['custom_meta_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom meta tags prefix'),
      '#description' => $this->t('Use this to define the prefix of the custom meta tags.'),
      '#default_value' => $this->config('custom_meta.settings')->get('prefix') ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('custom_meta.settings');
    $settings->set('prefix', $form_state->getValue('custom_meta_prefix'));
    $settings->save();
    parent::submitForm($form, $form_state);
  }

}
