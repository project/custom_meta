<?php

namespace Drupal\custom_meta\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a custom meta tag.
 */
class DeleteForm extends ConfirmFormBase {

  /**
   * The custom meta tag being deleted.
   *
   * @var array
   */
  protected $customMeta;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_meta_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete custom meta tag %title?', ['%title' => $this->customMeta['label']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('custom_meta.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $values = $this->config('custom_meta.settings')->get('tag');

    $this->customMeta = $values[$id];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $this->config('custom_meta.settings')->get('tag');
    if ($form_state->getValue('confirm')) {
      unset($values[$this->customMeta['name']]);
      $this->configFactory()->getEditable('custom_meta.settings')
        ->set('tag', $values)
        ->save();
    }
    $form_state->setRedirect('custom_meta.admin_overview');
  }

}
