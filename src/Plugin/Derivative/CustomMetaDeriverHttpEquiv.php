<?php

namespace Drupal\custom_meta\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Create a new custom_meta tag plugin for http-equiv custom tags.
 */
class CustomMetaDeriverHttpEquiv extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Get a list of all custom defined metas.
    $custom_meta_settings = \Drupal::config('custom_meta.settings');
    $custom_meta = $custom_meta_settings->get('tag') ?? [];
    $custom_meta_prefix = $custom_meta_settings->get('prefix') ?? '';
    // Now we loop over them and declare the derivatives.
    foreach ($custom_meta as $id => $meta) {
      if ($meta['attribute'] == 'http-equiv') {
        // The base definition includes the annotations defined in the plugin,
        // i.e. CustomMetaTagHttpEquiv. Each one may be overridden.
        $derivative = $base_plugin_definition;

        // Here we fill in any missing keys on the layout annotation.
        $derivative['weight']++;
        $derivative['id'] = $id;
        $derivative['name'] = $custom_meta_prefix . $id;
        $derivative['label'] = t($meta['label']);
        $derivative['description'] = t($meta['description']);

        // Reference derivatives based on their UUID instead of the record ID.
        $this->derivatives[$derivative['id']] = $derivative;
      }
    }

    return $this->derivatives;
  }

}
