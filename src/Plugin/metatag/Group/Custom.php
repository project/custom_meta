<?php

namespace Drupal\custom_meta\Plugin\metatag\Group;

use Drupal\metatag\Plugin\metatag\Group\GroupBase;

/**
 * The custom meta tag group.
 *
 * @MetatagGroup(
 *   id = "custom_meta",
 *   label = @Translation("Custom Metatags"),
 *   description = @Translation("These custom meta tags are designed to use the custom purpose on the website."),
 *   weight = 3
 * )
 */
class Custom extends GroupBase {
  // Inherits everything from Base.
}
