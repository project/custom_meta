<?php

namespace Drupal\custom_meta\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaHttpEquivBase;

/**
 * Custom configured meta tags will be available.
 *
 * The meta tag's values will be based upon this annotation.
 *
 * @MetatagTag(
 *   id = "custom_meta_tag_http_equiv",
 *   deriver = "Drupal\custom_meta\Plugin\Derivative\CustomMetaDeriverHttpEquiv",
 *   label = @Translation("Custom http-equiv Meta tag"),
 *   description = @Translation("This plugin will be cloned from these settings for each custom tag."),
 *   name = "custom_meta_tag_http_equiv",
 *   weight = 3,
 *   group = "custom_meta",
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class CustomMetaTagHttpEquiv extends MetaHttpEquivBase {

  /**
   * {@inheritdoc}
   */
  public function output(): array {
    $element = parent::output();
    // Unset empty values.
    if (!empty($element)) {
      foreach ($element as $key => $item) {
        if ($item['#attributes'] && empty($item['#attributes']['content'])) {
          unset($element[$key]);
        }
      }
    }
    return $element;
  }

}
