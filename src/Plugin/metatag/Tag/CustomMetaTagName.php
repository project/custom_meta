<?php

namespace Drupal\custom_meta\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * Custom configured meta tags will be available.
 *
 * The meta tag's values will be based upon this annotation.
 *
 * @MetatagTag(
 *   id = "custom_meta_tag_name",
 *   deriver = "Drupal\custom_meta\Plugin\Derivative\CustomMetaDeriverName",
 *   label = @Translation("Custom name Meta tag"),
 *   description = @Translation("This plugin will be cloned from these settings for each custom tag."),
 *   name = "custom_meta_tag_name",
 *   weight = 1,
 *   group = "custom_meta",
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class CustomMetaTagName extends MetaNameBase {

  /**
   * {@inheritdoc}
   */
  public function output(): array {
    $element = parent::output();
    // Unset empty values.
    if (!empty($element)) {
      foreach ($element as $key => $item) {
        if ($item['#attributes'] && empty($item['#attributes']['content'])) {
          unset($element[$key]);
        }
      }
    }
    return $element;
  }

}
