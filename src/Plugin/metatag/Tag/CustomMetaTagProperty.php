<?php

namespace Drupal\custom_meta\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaPropertyBase;

/**
 * Custom configured meta tags will be available.
 *
 * The meta tag's values will be based upon this annotation.
 *
 * @MetatagTag(
 *   id = "custom_meta_tag_property",
 *   deriver = "Drupal\custom_meta\Plugin\Derivative\CustomMetaDeriverProperty",
 *   label = @Translation("Custom property Meta tag"),
 *   description = @Translation("This plugin will be cloned from these settings for each custom tag."),
 *   name = "custom_meta_tag_property",
 *   weight = 2,
 *   group = "custom_meta",
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class CustomMetaTagProperty extends MetaPropertyBase {

  /**
   * {@inheritdoc}
   */
  public function output(): array {
    $element = parent::output();
    // Unset empty values.
    if (!empty($element)) {
      foreach ($element as $key => $item) {
        if ($item['#attributes'] && empty($item['#attributes']['content'])) {
          unset($element[$key]);
        }
      }
    }
    return $element;
  }

}
