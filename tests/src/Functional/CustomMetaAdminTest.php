<?php

namespace Drupal\Tests\custom_meta\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Custom Meta administration.
 *
 * @group custom_meta
 */
class CustomMetaAdminTest extends BrowserTestBase {

  use CustomMetaHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'metatag',
    'custom_meta',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this
      ->drupalCreateUser([
        'administer custom meta tags',
      ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the custom meta administration end to end.
   */
  public function testCustomMeta() {
    // Update custom meta settings without prefix.
    $this->updateCustomMetaSettingsWithoutPrefix();
    // Remove default custom meta tags.
    $this->removeDefaultCustomMetaTags();
    // Check custom meta listing empty text.
    $this->customMetaListingEmptyText();
    // Access custom meta listing page operations for name type.
    $this->customMetaListingPageOperations('name');
    // Access custom meta listing page operations for property type.
    $this->customMetaListingPageOperations('property');
    // Access custom meta listing page operations for http-equiv type.
    $this->customMetaListingPageOperations('http-equiv');
  }

  /**
   * Test custom meta listing page operations.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function customMetaListingPageOperations($attribute) {
    // Create custom meta tag.
    $this->createCustomMetaTag($attribute);
    // Update custom meta tag.
    $this->updateCustomMetaTag();
    // Delete custom meta tag.
    $this->deleteCustomMetaTag();
  }

}
