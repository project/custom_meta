<?php

namespace Drupal\Tests\custom_meta\Functional;

/**
 * Custom Meta Helper functions for the automated tests.
 */
trait CustomMetaHelperTrait {

  /**
   * Update custom meta settings without prefix.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function updateCustomMetaSettingsWithoutPrefix() {
    $this->drupalGet('admin/config/search/metatag/custom-meta/settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->addressEquals('/admin/config/search/metatag/custom-meta/settings');
  }

  /**
   * Update custom meta settings with prefix.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function updateCustomMetaSettingsWithPrefix() {
    $this->drupalGet('admin/config/search/metatag/custom-meta/settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['custom_meta_prefix' => 'prefix_'], 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->addressEquals('/admin/config/search/metatag/custom-meta/settings');
  }

  /**
   * Create Custom Metatag.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function createCustomMetaTag($attribute) {
    // Access custom meta add page.
    $this->drupalGet('admin/config/search/metatag/custom-meta/add');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [];
    $edit['attribute'] = $attribute;
    $edit['name'] = 'foo';
    $edit['label'] = 'foo label';
    $edit['description'] = 'foo description';
    $this->submitForm($edit, 'Save');
    $this->assertSession()->addressEquals('/admin/config/search/metatag/custom-meta');
    $this->assertSession()->pageTextContains('Meta tag has been saved.');
  }

  /**
   * Update Custom Metatag.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function updateCustomMetaTag() {
    $this->drupalGet('admin/config/search/metatag/custom-meta/edit/foo');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['description' => 'foo description updated'], 'Save');
    $this->assertSession()->addressEquals('/admin/config/search/metatag/custom-meta');
    $this->assertSession()->pageTextContains('Meta tag has been saved.');
  }

  /**
   * Delete Custom Metatag.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function deleteCustomMetaTag() {
    $this->drupalGet('admin/config/search/metatag/custom-meta/delete/foo');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Are you sure you want to delete custom meta tag foo label?');
    $this->submitForm([], 'Confirm');
    $this->assertSession()->addressEquals('/admin/config/search/metatag/custom-meta');
  }

  /**
   * Remove default custom meta tags.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function removeDefaultCustomMetaTags() {
    $this->drupalGet('admin/config/search/metatag/custom-meta/delete/sitename');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Are you sure you want to delete custom meta tag Sitename?');
    $this->submitForm([], 'Confirm');
    $this->assertSession()->addressEquals('/admin/config/search/metatag/custom-meta');
  }

  /**
   * Check custom meta listing page empty text.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function customMetaListingEmptyText() {
    $this->drupalGet('admin/config/search/metatag/custom-meta');
    $this->assertSession()->statusCodeEquals(200);
    // Check that the Add tag link exists.
    $this->assertSession()->linkByHrefExists('admin/config/search/metatag/custom-meta/add');
    // Check that empty message exists.
    $this->assertSession()->pageTextContains('No custom meta tags available.');
  }

}
