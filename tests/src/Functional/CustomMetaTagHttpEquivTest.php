<?php

namespace Drupal\Tests\custom_meta\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that verify http-equiv attribute custom tags work correctly.
 *
 * @group custom_meta
 */
class CustomMetaTagHttpEquivTest extends BrowserTestBase {

  use CustomMetaHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'metatag',
    'custom_meta',
    'metatag_test_custom_route',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $permissions = [
      'administer site configuration',
      'administer meta tags',
      'administer custom meta tags',
      'access content',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the custom meta tag http-equiv without prefix.
   */
  public function testCustomMetaTagHttpEquivWithoutPrefix() {
    // Access custom meta settings page.
    $this->updateCustomMetaSettingsWithoutPrefix();
    // Perform custom meta tag add operation from custom meta listing page.
    $this->createCustomMetaTag('http-equiv');
    // Rebuild cache.
    $this->rebuildAll();
    // Save the value into the custom meta tag.
    $this->drupalGet('/admin/config/search/metatag/global');
    $this->submitForm(['custom_meta_tag_http_equiv:foo' => 'foo value'], 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Saved the Global Metatag defaults.');
    // Load the metatag custom route to verify custom metatag exists.
    $this->drupalGet('/metatag_test_custom_route');
    $this->assertSession()->elementExists('xpath', '//meta[@http-equiv="foo" and @content="foo value"]');
  }

  /**
   * Tests the custom meta tag http-equiv with prefix.
   */
  public function testCustomMetaTagHttpEquivWithPrefix() {
    // Access custom meta settings page.
    $this->updateCustomMetaSettingsWithPrefix();
    // Perform custom meta tag add operation from custom meta listing page.
    $this->createCustomMetaTag('http-equiv');
    // Rebuild cache.
    $this->rebuildAll();
    // Save the value into the custom meta tag.
    $this->drupalGet('/admin/config/search/metatag/global');
    $this->submitForm(['custom_meta_tag_http_equiv:foo' => 'foo value'], 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Saved the Global Metatag defaults.');
    // Load the metatag custom route to verify custom metatag exists.
    $this->drupalGet('/metatag_test_custom_route');
    $this->assertSession()->elementExists('xpath', '//meta[@http-equiv="prefix_foo" and @content="foo value"]');
  }

}
