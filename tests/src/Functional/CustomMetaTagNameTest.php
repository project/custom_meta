<?php

namespace Drupal\Tests\custom_meta\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that verify name attribute custom tags work correctly.
 *
 * @group custom_meta
 */
class CustomMetaTagNameTest extends BrowserTestBase {

  use CustomMetaHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'metatag',
    'custom_meta',
    'metatag_test_custom_route',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $permissions = [
      'administer site configuration',
      'administer meta tags',
      'administer custom meta tags',
      'access content',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the custom meta tag name without prefix.
   */
  public function testCustomMetaTagNameWithoutPrefix() {
    // Access custom meta settings page.
    $this->updateCustomMetaSettingsWithoutPrefix();
    // Perform custom meta tag add operation from custom meta listing page.
    $this->createCustomMetaTag('name');
    // Rebuild cache.
    $this->rebuildAll();
    // Save the value into the custom meta tag.
    $this->drupalGet('/admin/config/search/metatag/global');
    $this->submitForm(['custom_meta_tag_name:foo' => 'foo value'], 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Saved the Global Metatag defaults.');
    // Load the metatag custom route to verify custom metatag exists.
    $this->drupalGet('/metatag_test_custom_route');
    $this->assertSession()->elementExists('xpath', '//meta[@name="foo" and @content="foo value"]');
  }

  /**
   * Tests the custom meta tag name with prefix.
   */
  public function testCustomMetaTagNameWithPrefix() {
    // Access custom meta settings page.
    $this->updateCustomMetaSettingsWithPrefix();
    // Perform custom meta tag add operation from custom meta listing page.
    $this->createCustomMetaTag('name');
    // Rebuild cache.
    $this->rebuildAll();
    // Save the value into the custom meta tag.
    $this->drupalGet('/admin/config/search/metatag/global');
    $this->submitForm(['custom_meta_tag_name:foo' => 'foo value'], 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Saved the Global Metatag defaults.');
    // Load the metatag custom route to verify custom metatag exists.
    $this->drupalGet('/metatag_test_custom_route');
    $this->assertSession()->elementExists('xpath', '//meta[@name="prefix_foo" and @content="foo value"]');
  }

}
